namespace System.Web
{
    public static class KeyInfo
    {

        public const string PublicKeyToken = "b213442182ae2f7a";
        public const string PublicKey = "00240000048000009400000006020000002400005253413100040000010001002f6e9a368e9e01498770510511c4eca94a7587261f4e495ccd18d41b7c3fa92826a621c30dfc91f5e8a0a0ef4681dc7e4136f4bff4f96cfbb3a7acf70051fa870d27884439b4ceed676dd99c7e8618a6c7a002252c587e51d6df603722a43ed32597c0ba8ecd9a808d6fbe1cd8914680a5a7e91f57ff33b90764ab0aec7131ba";
        public const string commaPublicKey = ", PublicKey=" + PublicKey;

    }
}